package app.services;

import app.DAO.UserDao;
import app.Entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service("userService")
@Transactional
public class UserServiceImpl implements UserService{

    @Autowired
    private UserDao dao;

    @Override
    public void create(User user) {
        dao.create(user);
    }

    @Override
    public void update(User user) {
        dao.update(user);
    }

    @Override
    public void remove(User user) {
        dao.remove(user);
    }

    @Override
    public List<User> findAll() {
        return dao.findAll();
    }

    @Override
    public User findByLogin(String login) {
        return dao.findByLogin(login);
    }

    @Override
    public User findByEmail(String email) {
        return dao.findByEmail(email);
    }

}
