package app.controller;

import app.Entity.Role;
import app.Entity.User;
import app.services.RoleService;
import app.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;

@Controller
@ComponentScan("app")
public class UserManagerServlet {

    @Autowired
    UserService userService;
    @Autowired
    RoleService roleService;
    @Autowired
    MessageSource messageSource;

    @RequestMapping(value = {"/add"})
    protected ModelAndView add(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        User user = new User();
        Role role = new Role();
        user.setRole(role);
        final String act = req.getParameter("act");
        User userchack = userService.findByLogin(req.getParameter("username"));

        if (userchack != null) {
            return new ModelAndView("/sendonaddchange?act=Add");
        }

        initUser(user, req, role);

        if (!isValidPassword(req, resp, act, user)) {
            return new ModelAndView("/sendonaddchange?act=Add");
        }


        if (userchack == null) {
            userService.create(user);

//            req.setAttribute("userService", userService);// throw service
//            resp.sendRedirect("/homeadmin.jsp");
            return new ModelAndView("homeadmin", "userService", userService);
        }
        return new ModelAndView("/sendonaddchange?act=Add");

    }

    @RequestMapping(value = {"/edit"})
    protected ModelAndView doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        User user = new User();
        Role role = new Role();
        user.setRole(role);
        final String act = req.getParameter("act");
        User userchack = userService.findByLogin(req.getParameter("username"));

        initUser(user, req, role);

        user.setId(userchack.getId());
        userService.update(user);
        return new ModelAndView("homeadmin", "userService", userService);

    }

    boolean isValidPassword(HttpServletRequest req, HttpServletResponse resp, String act, User user) throws ServletException, IOException {
        if (!req.getParameter("password").equals(req.getParameter("passwordagain")) || !isDateValid(req.getParameter("birthday"))) {
            req.setAttribute("act", act);
            req.setAttribute("user", user);
            req.getRequestDispatcher("/addchangeuser.jsp").forward(req, resp);
            return false;
        }
        return true;
    }

    @RequestMapping(value = {"/del"})
    protected ModelAndView delete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String login = req.getParameter("login");
        User user = userService.findByLogin(login);
        userService.remove(user);
//        resp.sendRedirect("/homeadmin.jsp");
        return new ModelAndView("homeadmin", "userService", userService);
    }

    public static boolean isDateValid(String date) {
        SimpleDateFormat myFormat = new SimpleDateFormat("dd.MM.yyyy");
        myFormat.setLenient(false);
        try {
            myFormat.parse(date);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    private void initUser(User user, HttpServletRequest req, Role role) {
        user.setLogin(req.getParameter("username"));
        user.setPassword(req.getParameter("password"));
        user.setEmail(req.getParameter("email"));
        user.setFirstName(req.getParameter("firstname"));
        user.setLastName(req.getParameter("lastname"));
        user.setBirthDay(req.getParameter("birthday"));
        role = roleService.findByName(req.getParameter("role"));
        user.setRole(role);
    }
}
