package app.controller;

import app.Entity.User;
import app.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@Controller
public class UserManagerFormProvider{

    @Autowired
    UserService userService;

    @RequestMapping(value = {"/sendonaddchange"})
    protected void sendonaddchange(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        final String act = req.getParameter("act");
        final String login = req.getParameter("login");
        if (act.equals("Add")) {
            req.setAttribute("act", act);
            req.getRequestDispatcher("/addchangeuser.jsp").forward(req, resp);
        }
        if (act.equals("Edit")) {
            User user = userService.findByLogin(login);
            req.setAttribute("act", act);
            req.setAttribute("user", user);
            req.getRequestDispatcher("/addchangeuser.jsp").forward(req, resp);
        }
    }
}
